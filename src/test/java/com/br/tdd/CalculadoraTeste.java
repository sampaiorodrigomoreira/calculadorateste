package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    @Test
    public void testarSomaDeDoisNumeros(){
        int resultado = Calculadora.soma(2,2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarSubtracaoDeDoisNumeros(){
        int resultado = Calculadora.subtrair(3,1);
        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testarSubNumerosQuebrados(){
        double resultado = Calculadora.subtrair(2.2, 0.1);
        Assertions.assertEquals(2.1, resultado);
    }

    @Test
    public void testarDivNumerosQuebrados(){
        double resultado = Calculadora.divisao(0.0, 0.1);
        Assertions.assertEquals(0.0, resultado);
    }

    @Test
    public void testarDivNumerosFlutuantes(){
        float resultado = (float) Calculadora.divNumeroFlutuantes(10.0f, 2.0f);
        Assertions.assertEquals(5.0, resultado);
    }

    @Test
    public void testarMulNumerosQuebrados(){
        double resultado = Calculadora.multiplicacao(2.2, 0.1);
        Assertions.assertEquals(2.1, resultado);
    }
}
