package com.br.tdd;

public class Calculadora {


    public static int soma(int num1, int num2){
        int resultado = num1 + num2;
        return resultado;
    }

    public static double subtrair(double num1, double num2){
        double resultado = num1 - num2;
        return resultado;
    }

    public static int subtrair(int num1, int num2){
        int resultado = num1 - num2;
        return resultado;
    }

    public static double divisao(double num1, double num2){
        double resultado = num1 / num2;
        return resultado;
    }

    public static double divNumeroFlutuantes(float num1, float num2){
        float resultado = num1 / num2;
        System.out.println("resultado: " + resultado);
        return resultado;
    }


    public static double multiplicacao(double num1, double num2){
        double resultado = num1 * num2;
        return resultado;
    }

}
